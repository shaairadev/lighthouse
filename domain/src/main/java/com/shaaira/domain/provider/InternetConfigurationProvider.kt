package com.shaaira.domain.provider

import com.shaaira.domain.connection.ConfigurationInteractor
import com.shaaira.domain.connection.WebConfigurationInteractor
import com.shaaira.web.provider.IPConfigurationProvider

interface ConfigurationProvider {
    fun provideConnection(): ConfigurationInteractor
}

internal class InternetConfigurationProvider(
    private val ipConfigurationProvider: IPConfigurationProvider
) : ConfigurationProvider {
    override fun provideConnection(): ConfigurationInteractor {
        return WebConfigurationInteractor(ipConfigurationProvider.provideIPConfiguration())
    }
}

