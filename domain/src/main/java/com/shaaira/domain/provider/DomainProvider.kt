package com.shaaira.domain.provider

import android.content.Context
import com.shaaira.domain.connection.ConfigurationInteractor
import com.shaaira.web.provider.IPConfigurationProvider
import com.shaaira.web.provider.IPConfigurationProviderImpl

//FIXME: DI instead of this
class DomainDependenciesProvider private constructor(context: Context) {

    private val ipConfigurationProvider: IPConfigurationProvider =
        IPConfigurationProviderImpl(context)
    private val configurationProvider: ConfigurationProvider =
        InternetConfigurationProvider(ipConfigurationProvider)

    fun provideConfigurationInteractor(): ConfigurationInteractor =
        configurationProvider.provideConnection()

    companion object {
        fun instance(context: Context): DomainDependenciesProvider =
            DomainDependenciesProvider(context)
    }
}