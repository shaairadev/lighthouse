package com.shaaira.domain.connection

import com.shaaira.web.configuration.WebConfiguration

class WebConfigurationReverter(webConfiguration: WebConfiguration) :
    WebConfigurationDecorator(webConfiguration) {
    override suspend fun getInfo(): String =
        wrappedConfiguration.getInfo().reversed()
}