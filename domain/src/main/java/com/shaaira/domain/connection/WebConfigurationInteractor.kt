package com.shaaira.domain.connection

import com.shaaira.web.configuration.WebConfiguration
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class WebConfigurationInteractor(
    private val webConfiguration: WebConfiguration
) : ConfigurationInteractor {

    override suspend fun getConfiguration(): String =
        getConfigurationInfo()


    private suspend fun getConfigurationInfo() = withContext(Dispatchers.IO) {
        webConfiguration.getInfo()
    }
}