package com.shaaira.domain.connection

interface ConfigurationInteractor {
    suspend fun getConfiguration(): String
}