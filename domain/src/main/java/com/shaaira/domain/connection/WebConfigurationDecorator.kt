package com.shaaira.domain.connection

import com.shaaira.web.configuration.WebConfiguration

abstract class WebConfigurationDecorator(protected val wrappedConfiguration: WebConfiguration) :
    WebConfiguration