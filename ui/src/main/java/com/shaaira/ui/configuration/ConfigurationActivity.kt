package com.shaaira.ui.configuration

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shaaira.ui.R
import kotlinx.android.synthetic.main.activity_configuration.*

internal class ConfigurationActivity : AppCompatActivity(), ConfigurationView {
    private lateinit var presenter: ConfigurationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration)
        presenter = ConfigurationPresenterImpl(this)

        btnLoad.setOnClickListener { loadConfiguration() }
        loadConfiguration()
    }

    private fun loadConfiguration() {
        txtInfo.text = getString(R.string.common_loading)
        presenter.loadConfigurationInformation(::showConnectionConfiguration)
    }

    private fun showConnectionConfiguration(configuration: String) {
        txtInfo.text = configuration
    }


}