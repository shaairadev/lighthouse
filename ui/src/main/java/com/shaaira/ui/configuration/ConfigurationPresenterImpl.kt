package com.shaaira.ui.configuration

import android.content.Context
import com.shaaira.domain.provider.DomainDependenciesProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

internal class ConfigurationPresenterImpl(context: Context) :
    ConfigurationPresenter {
    //FIXME:DI
    private val configurationInteractor =
        DomainDependenciesProvider.instance(context).provideConfigurationInteractor()

    override fun loadConfigurationInformation(show: (data: String) -> Unit) {
        CoroutineScope(Dispatchers.Main).launch {
            show.invoke(configurationInteractor.getConfiguration())
        }
    }

}