package com.shaaira.ui.configuration

interface ConfigurationPresenter {
    //FIXME: Move context provision to the DI
    fun loadConfigurationInformation(show: (data: String) -> Unit)
}