package com.shaaira.web.configuration

import android.util.Log
import com.shaaira.web.ip.IPService
import java.io.IOException

internal class LocalWebConfiguration(private val ipService: IPService) : WebConfiguration {
    override suspend fun getInfo(): String {
        return try {
            val addressAsString = ipService.getIP()
            if (isIPv4Format(addressAsString)) {
                addressAsString
            } else {
                formatToIPv4(addressAsString)
            }
        } catch (error: IOException) {
            Log.e("WebConfiguration", "Can't get IP", error)
            error.message ?: "Can't get IP"
        }
    }


    private fun isIPv4Format(address: String) = address.indexOf(':') < 0

    private fun formatToIPv4(address: String): String {
        val delimiter = address.indexOf('%')
        return if (delimiter <= 0) {
            address
        } else {
            address.substring(0, delimiter)
        }
    }
}

