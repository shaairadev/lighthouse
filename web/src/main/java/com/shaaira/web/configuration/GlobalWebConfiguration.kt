package com.shaaira.web.configuration

import android.util.Log
import com.shaaira.web.ip.IPService

internal class GlobalWebConfiguration(private val ipWebService: IPService) : WebConfiguration {
    override suspend fun getInfo(): String {
        val ipFromWeb = ipWebService.getIP()
        Log.v("IPConfiguration", "IP from web: '$ipFromWeb'")
        return ipFromWeb
    }

}