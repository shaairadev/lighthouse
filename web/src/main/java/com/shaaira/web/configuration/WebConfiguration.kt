package com.shaaira.web.configuration

interface WebConfiguration {
    suspend fun getInfo(): String
}