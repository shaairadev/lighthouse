package com.shaaira.web.network

import android.content.Context
import android.net.ConnectivityManager
import java.io.IOException
import java.net.HttpURLConnection

@Suppress("DEPRECATION")
internal class AndroidNetworkManager(private val context: Context) : NetworkManager {
    override fun ensureNetworkIsReachable() {
        val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = manager.activeNetworkInfo

        if (networkInfo == null || !networkInfo.isAvailable || !networkInfo.isConnected) {
            throw IOException("Network is not connected")
        }
    }

    override fun isResponseValid(connection: HttpURLConnection) =
        connection.responseCode == HttpURLConnection.HTTP_OK
}