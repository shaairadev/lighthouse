package com.shaaira.web.network

import android.util.Log
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

internal class URLClient {
    fun connectByURL(url: String): HttpURLConnection {
        try {
            val connection: HttpURLConnection =
                URL(url).openConnection() as HttpURLConnection
            connection.setRequestProperty(
                FIRST_REQUEST_PROPERTY_KEY,
                FIRST_REQUEST_PROPERTY_VALUE
            )
            connection.setRequestProperty(
                SECOND_REQUEST_PROPERTY_KEY,
                SECOND_REQUEST_PROPERTY_VALUE
            )
            connection.requestMethod = "GET"
            connection.readTimeout = 30000
            connection.connectTimeout = 30000
            connection.connect()
            return connection
        } catch (error: java.lang.Exception) {
            Log.e("IPWebService", "Could not connect to URL: $error")
            throw IOException("Network is not connected")
        }
    }

    companion object {
        const val FIRST_REQUEST_PROPERTY_KEY = "User-Agent"
        const val FIRST_REQUEST_PROPERTY_VALUE = "Android-device"
        const val SECOND_REQUEST_PROPERTY_KEY = "Content-type"
        const val SECOND_REQUEST_PROPERTY_VALUE = "application/json"
    }
}