package com.shaaira.web.network

import java.net.HttpURLConnection

internal interface NetworkManager {
    fun ensureNetworkIsReachable()
    fun isResponseValid(connection: HttpURLConnection): Boolean
}