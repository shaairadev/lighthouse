package com.shaaira.web.ip

import java.net.InetAddress

fun InetAddress.formattedHostAddress() = hostAddress.replace("/", "")