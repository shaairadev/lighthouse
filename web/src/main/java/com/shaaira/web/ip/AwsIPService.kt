package com.shaaira.web.ip

import com.shaaira.web.network.NetworkManager
import com.shaaira.web.network.URLClient
import com.shaaira.web.steam.StreamReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.HttpURLConnection

internal class AwsIPService(
    private val networkManager: NetworkManager,
    private val streamReader: StreamReader
) : IPService {
    override suspend fun getIP(): String = try {
        networkManager.ensureNetworkIsReachable()
        sendHTTPRequest()
    } catch (error: IOException) {
        error.message ?: "Network is not reachable"
    }

    private suspend fun sendHTTPRequest() =
        withContext(Dispatchers.IO) {
            val openConnection = URLClient().connectByURL(URL_STRING)
            val data = getHttpData(openConnection)
            openConnection.disconnect()
            data
        }

    private fun getHttpData(urlConnection: HttpURLConnection) =
        if (networkManager.isResponseValid(urlConnection)) {
            val result = streamReader.readStream(urlConnection.inputStream)
            result
        } else {
            throw IOException("Network is not connected")
        }

    companion object {
        const val URL_STRING = "https://checkip.amazonaws.com/"
    }
}