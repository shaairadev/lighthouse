package com.shaaira.web.ip

interface IPService {
    suspend fun getIP(): String
}

