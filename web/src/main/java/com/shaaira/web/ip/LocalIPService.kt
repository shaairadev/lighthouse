package com.shaaira.web.ip

import android.util.Log
import java.io.IOException
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*

internal class LocalIPService : IPService {

    override suspend fun getIP(): String = getAddressWithoutLoopback().hostAddress

    private fun getAddressWithoutLoopback(): InetAddress =
        scanNetworkInterfacesForIPAddress(NetworkInterface.getNetworkInterfaces())
            ?: throw IOException("IP address could not be provided")


    private fun scanNetworkInterfacesForIPAddress(interfaces: Enumeration<NetworkInterface>): InetAddress? {
        for (networkInterface in interfaces) {
            logNetworkAddresses(networkInterface)
            return findLocalNetworkAddress(networkInterface) ?: continue
        }

        return null
    }

    private fun findLocalNetworkAddress(networkInterface: NetworkInterface): InetAddress? {
        for (address in networkInterface.inetAddresses) {
            if (isLocalNetworkAddress(address)) {
                return address
            }
        }

        return null
    }

    private fun isLocalNetworkAddress(address: InetAddress): Boolean =
        !address.isLoopbackAddress && hasMatchingPattern(address.formattedHostAddress())


    private fun hasMatchingPattern(address: String) =
        address.matches(Regex("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\$"))

    private fun logNetworkAddresses(networkInterface: NetworkInterface) {
        for (address in networkInterface.inetAddresses) {
            Log.d("IPConfiguration", "Address: $address")
        }
    }
}

