package com.shaaira.web.steam

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class StreamAppender : StreamReader {
    override fun readStream(stream: InputStream): String {
        val responseBuilder = StringBuilder()
        val reader = BufferedReader(InputStreamReader(stream))
        var line: String?

        while (reader.readLine().also { line = it } != null) {
            responseBuilder.append(line)
        }

        return responseBuilder.toString()
    }
}