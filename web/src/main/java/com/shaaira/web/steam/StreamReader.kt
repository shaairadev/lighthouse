package com.shaaira.web.steam

import java.io.InputStream

interface StreamReader {
    fun readStream(stream: InputStream): String
}