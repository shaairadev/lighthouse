package com.shaaira.web.provider

import android.content.Context
import com.shaaira.web.configuration.GlobalWebConfiguration
import com.shaaira.web.configuration.LocalWebConfiguration
import com.shaaira.web.configuration.WebConfiguration
import com.shaaira.web.ip.AwsIPService
import com.shaaira.web.ip.LocalIPService
import com.shaaira.web.network.AndroidNetworkManager
import com.shaaira.web.steam.StreamAppender

interface IPConfigurationProvider {
    fun provideIPConfiguration(): WebConfiguration
}

class IPConfigurationProviderImpl(private val context: Context) : IPConfigurationProvider {
    //TODO: Change by settings of application
    override fun provideIPConfiguration(): WebConfiguration =
        provideGlobalConfiguration()

    private fun provideLocalConfiguration() = LocalWebConfiguration(LocalIPService())

    private fun provideGlobalConfiguration() =
        GlobalWebConfiguration(AwsIPService(AndroidNetworkManager(context), StreamAppender()))
}